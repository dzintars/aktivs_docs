"""aktivs_docs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from docs.app.api import users as AppUsers
from docs.app.api import companies, storage
from docs.app.api import admin as ApiAdmin

apiPatterns = [
    path('login/', AppUsers.login),
    path('logout/', AppUsers.logout),
    path('token/refresh/', AppUsers.tokenRefresh),
    path('signup/', AppUsers.signup),
    path('signup/validate/', AppUsers.signupValidate),
    path('user/change_password', AppUsers.changePassword),
    path('user/', AppUsers.getData),
    path('company/', companies.getCompanyList),
    path('company/add/', companies.createCompany),
    path('company/update/', companies.updateCompany),
    path('company/<str:regno>/', companies.getCompany),
    path('document/<str:regno>/list', storage.fileList),
    path('document/<str:regno>/', storage.document),
    path('document/<str:regno>/<int:doc_id>', storage.document),
    path('document/<str:regno>/<int:doc_id>/download', storage.downloadDocumentFile),
    path('admin/gettext/<int:docId>', ApiAdmin.getText)
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(apiPatterns)),
    path('.well-known/', include('letsencrypt.urls')),
    path("",
        TemplateView.as_view(template_name="application.html"),
        name="app",
    ),
]
