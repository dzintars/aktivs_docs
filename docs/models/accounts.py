from django.db import models

#lietotāju konti
class Account(models.Model):
    subscription_valid = models.DateField() #datums līdz kuram derīgs abonements
    monthly_docs = models.IntegerField()    #dokumentu skaits cik lietotājs var mēnesī apstrādāt
    payment_reccurance = models.IntegerField(default = 12)  #mēnešu skaits abonementa atjaunošanai
    payer = models.CharField(max_length=35, blank = True) #maksātāja nosaukums
    payer_id = models.CharField(max_length=75, blank = True)  #maksātāja reģisrācija numurs
    payer_address = models.CharField(max_length=25, blank = True) #maksātāja adrese
    payer_vat_id = models.CharField(max_length=20, null = True) #maksātāja PVN numurs (ja ir)
    payer_email = models.EmailField(max_length=100, null = True, blank=True)  #maksātāja e-pasta adrese (rēķiniem)
    account_deleted = models.DateField(null = True, blank = True)   #konta dzēšana datums (null - konts derīgs, ja datums, tad konts nav derīgs)


#kontam piesaistītie uzņēmumi
class Companies(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)  #uzņēmumam piesaistītais konts
    company = models.CharField(max_length = 35) #uzņēmuma nosaukums
    regno = models.CharField(max_length = 25)   #uzņēmuma reģistrācijas numurs
    legal_address = models.CharField(max_length = 75)   #uzņēmuma juridiskā adrese
    vat_id = models.CharField(max_length = 20, null = True) #uzņēmuma PVN numurs
    added = models.DateField()  #datums, kad uzņēmums ir pievienots
    deleted = models.DateField(null = True, blank = True)   #datums, kad uzņēmums ir izdzēsts

#lietotāju saisaiste ar kontu
class AccountUsers(models.Model):
    account = models.ForeignKey(Account, on_delete = models.CASCADE)
    user = models.IntegerField()    #lietotāja ID
    account_admin = models.BooleanField(default = True) #lietotājs ir konta administrators
    valid = models.BooleanField(default = True) #lietotājs ir derīgs

#iestatījumi
class AccountSettings(models.Model):
    account = models.ForeignKey(Account, on_delete = models.CASCADE)
    webhook = models.CharField(max_length=250, blank = True)    #webhook ziņojumu adrese
    default_company = models.ForeignKey(Companies, null = True, on_delete = models.CASCADE) #noklusētais uzņēmums ar kuru sākt darbu
    
#konta lietojums
class AccountUsage(models.Model):
    account = models.ForeignKey(Account, on_delete = models.CASCADE)
    month = models.DateField()  #atskaites mēnesis
    docs_processed = models.IntegerField()  #apstrādāto dokumentu skaits


    






