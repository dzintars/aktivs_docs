from django.db import models

#Atjaunošanas talonu reģistrs
class RefreshTokens(models.Model):
    user_id = models.IntegerField()
    salt = models.CharField(max_length = 50)
    issued = models.DateField()
    
