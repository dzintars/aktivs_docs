from django.db import models
from docs.models.accounts import Account, Companies

#datu uzglabāšana
class Storage(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)  #konts
    filename = models.CharField(max_length = 75)    #datnes nosaukums
    file_path = models.CharField(max_length = 250)  #datnes atrašanās vieta
    file_size_kb = models.DecimalField(default = 0, max_digits=10, decimal_places=2)    #datnes izmērs
    content_type = models.CharField(max_length = 100)  #datnes tips
    added = models.DateTimeField(auto_now_add=True)  #datums/laiks, kad datnes pievienots
    deleted = models.DateTimeField(null = True) #datums/laiks, kad datnes ir dzēsts
    user_added = models.IntegerField()  #lietotājs, kurš pievienoja failu
    user_deleted = models.IntegerField(null = True) #lietotājs, kurš izdzēsa failu

#atpazītie dati
class Metadata(models.Model):
    account = models.ForeignKey(Account, on_delete = models.CASCADE)
    storage = models.ForeignKey(Storage, null=True, on_delete = models.SET_NULL)
    company = models.ForeignKey(Companies, on_delete=models.CASCADE) #uzņēmums
    json_data = models.TextField()  #atpazītie metadati
    proc_status = models.IntegerField()  #apstrādes statuss: 0: procesā: 1: pabeigts veiksmīgi; 2: kļūda; 3: kļūda (nepieciešama manuāla apskate)
    proc_status_change = models.DateTimeField(auto_now_add = True) #pēdējais notikuma maiņas datums
    export_status = models.IntegerField() #dati eksportēti uz ārēju sistēmu 0: nē/ 1: jā
    export_date = models.DateTimeField(null = True) #laiks, kad dati eksportēti uz ārēju sistēmu
    deleted = models.DateTimeField(null = True) #datums/laiks, kad datnes ir dzēsts
    user_deleted = models.IntegerField(null = True) #lietotājs, kurš izdzēsa failu
    
#darbību auditācija pieraksti
class SystemLog(models.Model):
    time = models.DateTimeField(auto_now_add=True, blank=True)  #notikuma laiks
    account_id = models.IntegerField(null = True)    #konts ar kuru saistīts notikums
    user_id = models.IntegerField(null = True) #lietotājs ar kuru saistīts notikums
    storage_id = models.IntegerField(null = True)   #datne ar kuru saistīts notikums
    metadata_id = models.IntegerField(null = True)    #atpazīto datu ieraksts 
    ip_address = models.CharField(max_length = 39, null = True) #ip adrese no kuras izsaukta darbība
    code = models.CharField(max_length = 15)    #auditācijas ieraksta darbības kods
    level = models.IntegerField()   #auditācijas ziņojuma svarīguma pakāpe: 1 - kritisks, 2 - brīdinājums, 3 - informācija
    info = models.CharField(max_length=100) 



