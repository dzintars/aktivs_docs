#darbības ar optisko rakstzīmju atpazīšanu
import os

#rakstzīmju atpazīšana
def performOcr(file_path, content_type):
    if content_type == "image/jpeg":
        #šobrīd neapstrādājam
        pass
    elif content_type == "application/pdf":
        #oriģinālā faila rezerves kopijas izveidošana
        backup_file = (os.path.splitext(file_path)[0]) + "_bck.pdf"
        os.rename(file_path,backup_file)
        os.system('ocrmypdf  -l eng+lav --pdfa-image-compression jpeg '+str(backup_file)+' '+str(file_path)+' --force-ocr')
    return file_path