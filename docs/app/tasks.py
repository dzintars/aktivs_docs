#asinhronie uzdevumi
from __future__ import absolute_import, unicode_literals
from django.conf import settings
from mailjet_rest import Client
from celery import task, shared_task
from .api.storage import getTextFromFile

#epasta nosūtīšana
@task(name="registration_email")
def sendEmail(name, email, code): 
    mailjet = Client(auth=(settings.MAILJET["API_KEY"], settings.MAILJET["API_SECRET"]), version='v3.1')
    data = {
    'Messages': [
            {
                "From": {
                    "Email": settings.MAILJET["MAIL_FROM_EMAIL"],
                    "Name": settings.MAILJET["MAIL_FROM"]
                },
                "To": [
                    {
                        "Email": email,
                        "Name": name
                    }
                ],
                "TemplateID": settings.MAILJET["TEMPLATE_ID"],
                "TemplateLanguage": True,
                "Subject": "E-pasta apstiprināšana",
                "Variables": {
                    "first_name": name,
                    "code": str(code)
                }
            }
        ]
    }
    result = mailjet.send.create(data=data)


#failu apstrādes asinhronā uzsākšana
@task(name="start_recognition_process")
def startRecognitionProcess(metadata_id):
    getTextFromFile(metadata_id)