#administratoram pieejamo darbību izsaukšana
from django.shortcuts import render, redirect
from django.http import JsonResponse
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from docs.app.api.decorators import token_required
from docs.models.system import *
from django.conf import settings
from datetime import date, datetime
from .errors import *
from .storage import getTextFromFile
from docs.models.system import Metadata

#teksta izgūšanas metodes manuāla izsaukšana (sinhrona izpilde)
#kļūdas statusa iestatīšana, ja metadatu izgūšana nav iespējama
@api_view(['GET', 'POST'])
@token_required
def getText(request, docId):
    try:
        user_id = request.token["user_id"]
        user = User.objects.get(id=user_id)
        if user.is_superuser == False:
            return error(401, "AUT_02")
    except:
        return error(401, "AUT_02")
    if request.method == 'GET':

        if request.GET.get("enable_ocr") is not None:
            return getTextFromFile(docId)
        else:
            return getTextFromFile(docId, disable_ocr = True)
    elif request.method == "POST":
        if (request.POST.get("action") is not None 
            and request.POST.get("action") == "set_error"):
            try:
                metadata = Metadata.objects.get(
                    id = docId, 
                    proc_status = 3
                )
                metadata.proc_status = 2
                metadata.proc_status_change = datetime.now()
                metadata.save()
                return JsonResponse({"stats": "error_status_set"})
            except:
                return error(400, "ERR_01")
        else:
            return error(400, "ERR_01")