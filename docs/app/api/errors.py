#api kļūdu atgriešana
from django.http import JsonResponse
from rest_framework.views import exception_handler

def error(status,code,field=""):
    errors = {
        "AUT_01": "Authorization code is empty.",
        "AUT_02": "Access Unauthorized.",
        "AUT_03": "Authorization code is incorrect.",
        "USR_01": "Email or Password is invalid.",
        "USR_02": "The field(s) are/is required.",
        "USR_03": "The email is invalid.",
        "USR_04": "The email already exists.",
        "USR_05": "The field '<FIELD>' is required.",
        "USR_06": "The field '<FIELD>' is invalid.",
        "USR_07": "The username already exists.",
        "USR_08": "Passwords are different",
        "USR_09": "The company already exists",
        "USR_10": "Company not found",
        "USR_11": "Password too short",
        "USR_12": "Current password is invalid",
        "FIL_01": "File not found",
        "FIL_02": "File not recognized",
        "FIL_03": "File content not recognized",
        "FIL_04": "File not available",
        "ERR_01": "Incorrect request"
    }
    response = {
        "status": status,
        "code": code,
        "message": errors[code].replace("<FIELD>", field),
        "field": field
    }

    return JsonResponse(response, status=status)