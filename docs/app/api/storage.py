from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.core.files import File
from rest_framework.decorators import api_view
from docs.app.api.decorators import token_required
from docs.models.accounts import Companies, AccountUsers
from docs.models.system import *
from django.conf import settings
from datetime import date, datetime
from .errors import *
from docs.app.api.companies import findCompany
from docs.app.ocr import performOcr
import os, pdftotext, yaml, regex, json, ast, img2pdf



#failu saraksta izgūšana
@api_view(['GET'])
@token_required
def fileList(request, regno):
    account_id = request.token["account_id"]
    company = findCompany(account_id, regno)
    if company == False: return error(400, "USR_10")
    today = datetime.now().date()
    if request.GET.get("date_from") is None:
        date_from = today
    else:
        try:
            date_from = datetime.strptime(request.GET.get("date_from"), '%Y-%m-%d')
        except:
            date_from = today
    if request.GET.get("date_to") is None:
        date_to = today
    else:
        try:
            date_to = datetime.strptime(request.GET.get("date_to"), '%Y-%m-%d')
        except:
            date_to = today    
    fileList = Metadata.objects.filter(
        account = account_id,
        company = company["id"],
        deleted__isnull = True,
        storage__added__date__gte = date_from,
        storage__added__date__lte = date_to
    ).values("id","storage__filename","storage__added","proc_status","proc_status_change").order_by("-id")
    result = []
    for f in fileList:
        result.append({
            "id": f["id"],
            "filename": f["storage__filename"],
            "date": f["storage__added"],
            "status": f["proc_status"],
            "status_change": f["proc_status_change"]
        })
    return JsonResponse({"list": result})

#failu augšupielādes apstrāde
def fileUpload(request, regno):
    from docs.app.tasks import startRecognitionProcess
    account_id = request.token["account_id"]
    account = Account.objects.get(id=int(account_id))
    company_id = findCompany(account_id, regno)
    if company_id == False: return error(400, "USR_10")
    company = Companies.objects.get(id=company_id["id"])
    allowed_content = ('application/pdf','image/jpeg')  #atļautie failu tipi
    max_allowed_file_size = 1 * 1024 * 1024 #Maksimālais atļautais viena faila izmērs. 1MB
    files = request.FILES.getlist('file')
    upload_status = []
    for f in files:
        #pārbaudam augušupielādēto failu saturu
        if f.content_type not in allowed_content:
            upload_status.append({
                "status": "error",
                "filename": str(f),
                "reason": "File type not allowed"
            })
            continue
        #pārbaudam failu izmēru
        if f.size > max_allowed_file_size:
            upload_status.append({
                "status": "error",
                "filename": str(f),
                "reason": "File too big (max " + str(max_allowed_file_size/(1024*1024)) + " MB)"
            })
            continue
        if len(f.name)>75:
            upload_status.append({
                "status": "error",
                "filename": str(f),
                "reason": "File name too long (max 75 char.)"
            })
            continue
        now = datetime.now()
        storage_path = settings.FILE_STORAGE_DIRECTORY + str(regno) + "/"   #faila saglabāšanas vieta
        try:
            os.makedirs(storage_path)
        except OSError as e:
            if e.errno == 17:
                #direktorija eksistē
                pass
        if f.content_type == "application/pdf":
            file_path = storage_path + str(datetime.timestamp(now)) + ".pdf"
        elif f.content_type == "image/jpeg":
            file_path = storage_path + str(datetime.timestamp(now)) + ".jpg"
        try:
            #saglabājam failu
            with open(file_path, 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)
            #saglabā datubāzē informāciju par failu
            db = Storage(
                account = account,
                filename = f.name.replace('"',''),
                file_path = file_path,
                file_size_kb = float(f.size/1024),
                content_type = f.content_type,
                user_added = int(request.token["user_id"])
                ) 
            db.save()
            file_id = db.id
            logger(
                code = "FILE_UPLOADED",
                request = request,
                account_id = account_id,
                user_id = request.token["user_id"],
                storage_id = file_id,
                log_level = 3
                )
            #izveido datubāzē metadatu sākotnējo ierakstu
            db2 = Metadata(
                account = account,
                storage = db,
                company = company,
                proc_status = 0,
                export_status = 0
            )
            db2.save()
            upload_status.append({
                "status": "success",
                "filename": str(f.name.replace('"','')),
                "id": db2.id
            })
            startRecognitionProcess.delay(db2.id)
        except Exception as e:
            upload_status.append({
                "status": "error",
                "filename": str(f.name.replace('"','')),
                "reason": "Incorrect request"
            })
            try:
                #ja kaut ko saglabājām, tad izdzēšam
                os.remove(file_path)
            except:
                pass
            logger(
                code = "FILE_UPLOAD_ERR",
                request = request,
                account_id = account_id,
                user_id = request.token["user_id"],
                log_level = 1,
                text = str(e)[:100]
            )
    return JsonResponse({"status": upload_status})

#audita pierakstu saglabāšana
def logger(code, request = None, account_id = None, user_id = None, storage_id = None, metadata_id = None, text = None, log_level = 3):
    log = SystemLog(
        level = log_level,
        code = code
    )
    if request is not None:
        ip = request.META.get('REMOTE_ADDR')
        log.ip_address = str(ip)
    if account_id is not None:
        log.account_id = int(account_id)
    if user_id is not None:
        log.user_id = int(user_id)
    if storage_id is not None:
        log.storage_id = int(storage_id)
    if metadata_id is not None:
        log.metadata_id = int(metadata_id)
    if text is not None:
        log.info = text
    log.save()
    return True
    
#izgūst tekstu no faila
#performOcr = False. Iestatījums attiecas tikai uz PDF dokumentu apstrādi.
#JPG rakstzīmju atpazīšana tiek izsaukta vienmēr
#PDF datu apstrādē tiek ignorēts, ja no PDF atgrieztā teksta virkne ir tukša (tiek ignorēts šis iestatījums)
#perform_ocr = True: pirms apstrādes uzsākšanas veic rakstzīmju atpazīšanu (rekursīvai izpildei, ja bez OCR izgūt datus neizdevās)
#disable_ocr = True: neveic rakstzīmju atpazīšanu neatkarīgi no tā, ka saturu atpazīt neizdodas
def getTextFromFile(metadata_id, perform_ocr = False, disable_ocr = False):
    try:
        metadata = Metadata.objects.filter(
            id = metadata_id,
            storage__deleted__isnull = True
            ).values("id","storage__id","storage__file_path","storage__content_type")[0]
    except:
        return error(400, "FIL_01")
    file_path = metadata["storage__file_path"]
    content_type = metadata["storage__content_type"]
    #pārbauda vai fails eksistē
    if os.path.exists(file_path) == False:
       logger(
            code = "PDF_TXT_ERR", 
            metadata_id = metadata["id"], 
            text="File not found", 
            log_level=1
            )
       return error(400, "FIL_01") 
    
    if perform_ocr == True:
        logger(
            code = "PDF_OCR_PROC", 
            metadata_id = metadata["id"], 
            text="OCR process started", 
            log_level=3
            )
        file_path = performOcr(file_path, content_type)
    if content_type == "image/jpeg":
        #pārveidojam JPEG par PDF un apstrādājam kā PDF
        with open(file_path+".pdf","wb") as f:
            f.write(img2pdf.convert(file_path))
        db = Storage.objects.get(
            id = metadata["storage__id"]
        )
        db.file_path = file_path + ".pdf"
        db.content_type = "application/pdf"
        db.save()
        return getTextFromFile(metadata_id, perform_ocr = True)
    elif content_type == "application/pdf":
        #atpazīstam tekstu
        with open(file_path, "rb") as f:
            pdf = pdftotext.PDF(f)
        #saglabājam atpazīto tekstu
        text_to_string = ""
        for page in pdf:
            text_to_string += page
        #failā teksts nav atrasts, nepieciešams veikt OCR
        if len(text_to_string)<=3:
            if perform_ocr == False and disable_ocr == False:
                #veicam rakstzīmju atpazīšanu
                return getTextFromFile(metadata_id, perform_ocr = True)
            logger(
                code = "PDF_TXT_ERR", 
                metadata_id = metadata["id"], 
                text="Text not found", 
                log_level=1
                )
            db = Metadata.objects.get(
                id = metadata_id
            )
            db.json_data = result
            db.proc_status = 3
            db.proc_status_change = datetime.now()
            db.save()     
            return error(400, "FIL_02")      
        with open(file_path+".txt", "w", encoding='utf-8') as f:
            for page in pdf:
                f.write(page)
            logger(
                code = "PDF_TXT", 
                metadata_id = metadata["id"], 
                text="Pdf to text success", 
                log_level=1
                )
        result = textToMetadata(metadata_id, metadata)
        if result == False and perform_ocr == False and disable_ocr == False:
            #izsaucam OCR atpazīšanu
            return getTextFromFile(metadata_id, perform_ocr = True)
        elif result == False:
            db = Metadata.objects.get(
                id = metadata_id
            )
            db.json_data = result
            db.proc_status = 2
            db.proc_status_change = datetime.now()
            db.save()
            return error(400, "FIL_03")
        if perform_ocr == True and result!=False:
            #izdzēšam faila rezerves kopiju, kura izveidota rakstzīmju atpazīšanas procesā
            try:
                backup_file = (os.path.splitext(file_path)[0]) + "_bck.pdf"
                os.remove(backup_file)
            except:
                pass
    else:
        logger(
            code = "PDF_TXT_ERR", 
            metadata_id = metadata["id"], 
            text="Wrong content type [" + str(content_type) + "]", 
            log_level=2
            )
        return error(400, "FIL_02")
    return result



#metadatu izgūšana no teksta faila
def textToMetadata(metadata_id, metadata = None):
    #izgūstam apstrādājamā faila atrašanās vietu (ja tā nav iedota)
    if metadata is None:
        try:
            metadata = Metadata.objects.filter(
                id = metadata_id,
                storage__deleted__isnull = True
                ).values("id","storage__file_path","storage__content_type")[0]
        except:
            return error(400, "FIL_01")
    #piemeklē sagatavi
    f = open(metadata["storage__file_path"]+".txt", "r", encoding='utf-8')
    text = f.read()
    #todo: atslēgas vārdus vajadzētu ielādēt atmiņā
    result = False
    for yaml_file_name in sorted(os.listdir(settings.DOC_TEMPLATE_DIRECTORY)):
        if yaml_file_name.endswith(".yaml") == False: continue
        with open(settings.DOC_TEMPLATE_DIRECTORY+yaml_file_name, encoding='utf-8') as f:
            yaml_templ = yaml.safe_load(f)
        #meklējam atslēgas vārdus tekstā
        keywords_found = regex.findall('|'.join(yaml_templ["keywords"]), text)
        #izņem dublikātus, ja tekstā ir vairākas viena vārda atbilstības
        keywords_found = list(dict.fromkeys(keywords_found))
        if len(keywords_found) == len(yaml_templ["keywords"]):
            result = useTemplate(yaml_templ,text,metadata_id,yaml_file_name)
            if result != False:
                break
    #sagatave nav atrasta
    #saglabājam audita pierakstu un statusu
    db = Metadata.objects.get(id=metadata_id)
    if result == False:
        logger(
            code = "TMPL_NOT_FOUND", 
            metadata_id = metadata_id, 
            text="Template not found", 
            log_level=1
            )
        return False
    else:
        db.json_data = result
        db.proc_status = 1
        db.proc_status_change = datetime.now()
        db.save() 
    return JsonResponse(result, safe=False)

#sagataves pielietošana
def useTemplate(yaml_templ,text,metadata_id,yaml_file_name):
    json_result = {}
    required_fields_found = True
    required_field_missing = ""
    #apstrādājam lauku definīcijas
    for field in yaml_templ["fields"]:
        result = regex.search(yaml_templ["fields"][field]["regex"], text)
        if result != None:
            result = result.group(0)
            result = formatRegexResult(result, yaml_templ["fields"][field]["type"])
        else:
            if yaml_templ["fields"][field]["required"] == True:
                required_field_missing = field 
                required_fields_found = False
                break
            continue
        if "prepend" in yaml_templ["fields"][field]:
            result = yaml_templ["fields"][field]["prepend"] + " " + result
        if "append" in yaml_templ["fields"][field]:
            result = result + " " + yaml_templ["fields"][field]["append"]
        json_result[field] = result
    #apstrādājam fiksētās vērtības
    if "fixed_fields" in yaml_templ and required_fields_found != False:
        for field in yaml_templ["fixed_fields"]:
            json_result[field] = yaml_templ["fixed_fields"][field]
    
    if required_fields_found:
        logger(
            code = "DATA_FOUND", 
            metadata_id = metadata_id, 
            text=("Template: " + str(yaml_file_name)), 
            log_level=3
            )
    else:
        logger(
            code = "REQ_FIELD_MISS", 
            metadata_id = metadata_id, 
            text=("Template: " + str(yaml_file_name)+", field: "+required_field_missing), 
            log_level=2
            )
        return False
    return json_result

#rezultāta formatēšana
def formatRegexResult(text, text_type):
    if text_type == "text":
        #sagaidāmais rezultāts ir teksts
        #tiek noņemtas liekās atstarpes teksta sākumā un beigās
        text = text.strip()
        return text
    if text_type == "date_text_lv":
        #datums teksta veidā latviešu valodā
        #Piemērs: 2020.gada 20.februāris -> 2020-02-20
        text_to_list = text.strip().split()
        year = int(text_to_list[0].replace('.',''))
        day = int(text_to_list[2].replace('.',''))
        month = monthToIntLv(text_to_list[3])
        return date(year, month, day).isoformat()
    if text_type == "date_ddmmyy":
        return date(
            2000+int(text.strip()[6:8]), 
            int(text.strip()[3:5]), 
            int(text.strip()[0:2])).isoformat()
    if text_type == "float":
        try:
            result = text.strip()
            result = result.replace(',','.')
            return float(result)
        except:
            return 0
    else:
        return text.strip()

#mēneša latviskā nosaukuma pārvēršana skaitlī
def monthToIntLv(month):
    if month[0:3] == 'jan':
        return 1
    elif month[0:3] == 'feb':
        return 2
    elif month[0:3] == 'mar':
        return 3
    elif month[0:3] == 'apr':
        return 4
    elif month[0:3] == 'mai':
        return 5
    elif month[0:3] == 'jūn':
        return 6
    elif month[0:3] == 'jūl':
        return 7
    elif month[0:3] == 'aug':
        return 8
    elif month[0:3] == 'sep':
        return 9
    elif month[0:3] == 'okt':
        return 10
    elif month[0:3] == 'nov':
        return 11
    elif month[0:3] == 'dec':
        return 12
    else:
        return 0
    
    
#darbs ar dokumentiem - pieprasījuma ieejas metode
@api_view(['GET','POST','PATCH','DELETE'])
@token_required
def document(request, regno, doc_id=-1):
    account_id = request.token["account_id"]
    company = findCompany(account_id, regno)
    if company == False: return error(400, "USR_10")
    if request.method == 'GET':
        #datu izgūšana
        if (doc_id == -1 and request.GET.get("id") is not None):
            doc_id = request.GET.get("id").split(',')
            response_array = True
        else:
            doc_id = [doc_id]
            response_array = False
        return getDocumentData(request, regno, doc_id, account_id, company, response_array = response_array)
    elif request.method == 'POST':
        return fileUpload(request, regno)
    elif request.method == 'DELETE':
        #datu dzēšana
        return deleteDocumentData(request, regno, doc_id, account_id, company)
    elif request.method == 'PATCH':
        #json datu labošana
        return updateDocumentData(request, regno, doc_id, account_id, company)
    else:
        return error(400, "ERR_01")

#dokumenta datu atgriešana
def getDocumentData(request, regno, doc_id, account_id, company, response_array = False):
    if request.GET.get("format") is not None and request.GET.get("format") == "tilde_inventory_xml":
        #TODO: datu atgriešana tildes Jumis formātā - Tildes rēķina forma
        pass
    elif request.GET.get("format") is not None and request.GET.get("format") == "tilde_accounting_xml":
        #TODO: datu atgriešana tildes Jumis formātā - Tildes grāmatojuma forma
        pass
    elif request.GET.get("format") is not None and request.GET.get("format") == "original":
        #TODO: attēla datnes atgriešana
        pass
    else:
        #atgriež JSON datus
        try: 
            metadata = Metadata.objects.filter(
                id__in = doc_id, 
                account = account_id,
                company = company["id"],
                deleted__isnull = True
            )
            result = []
            doc_id_found = []
            for m in metadata:
                if m.proc_status == 0:
                    #statuss - apstrādē
                    result.append({
                        "id": m.id, 
                        "status": "processing",
                        "status_change": m.proc_status_change   
                    })
                elif m.proc_status == 1:
                    #statuss - veiksmīga apstrāde, atgriežam rezultātu
                    res = m.json_data
                    res = ast.literal_eval(res) 
                    res = {
                        "id": m.id,
                        "status": "ready",
                        "status_change": m.proc_status_change,
                        "data": res
                    }
                    if (m.storage.deleted is None and 
                        m.storage.id is not None):
                        res["storage"] = {
                            "filename": m.storage.filename,
                            "url": "/api/document/"+str(regno)+"/"+str(m.id)+"/download",
                            "content_type": m.storage.content_type,
                            "file_size_kb": float(m.storage.file_size_kb)
                        }
                    result.append(res)
                elif m.proc_status == 2:
                    #statuss - kļūda, apstrāde netiks turpināta
                    result.append({
                        "id": m.id,
                        "status": "error",
                        "status_change": m.proc_status_change   
                    })
                elif m.proc_status == 3:
                    #statuss - pagaidu kļūda, nepieciešama datu manuāla apskate
                    result.append({
                        "id": m.id,
                        "status": "temporary_error",
                        "status_change": m.proc_status_change   
                    })
                doc_id_found.append(m.id)
            if (len(doc_id_found)!=len(doc_id)):
                for i in doc_id:
                    if int(i) not in doc_id_found:
                        result.append({
                            "id": i,
                            "status": "not_found"  
                        })
            if (response_array==False):
                result = result[0]
            return JsonResponse(result, safe=False)
        except:
            #dati netika atrasti
            for i in doc_id:
                logger(
                    code = "NOT_FOUND", 
                    request=request,
                    account_id = account_id,
                    metadata_id = i, 
                    user_id = request.token["user_id"],
                    log_level=2
                )
            return error(400, "FIL_04") 


#dokumenta datu atjaunošana
def updateDocumentData(request, regno, doc_id, account_id, company):
    try:
        metadata = Metadata.objects.get(
                    id = doc_id, 
                    account = account_id,
                    company = company["id"]
                )
    except:
        return error(400, "FIL_04") 
    result = metadata.json_data
    result = ast.literal_eval(result)
    keys_changed = []
    keys_added = []
    for key in request.POST:
        if key not in result:
            keys_added.append(key)
            result[key] = request.POST.get(key)
        elif result[key] != request.POST.get(key):
            keys_changed.append(key)
            result[key] = request.POST.get(key)
    if len(keys_changed) > 0 or len(keys_added) > 0:
        metadata.json_data = result
        metadata.save()
        text = ""
        if len(keys_changed) > 0:
            text = "Updated: " + (", ".join(keys_changed))
        if len(keys_added) > 0:
            if len(text) > 0: text += ", "
            text += "Added: " + (", ".join(keys_changed))
        logger(
            code = "DATA_UPDATED", 
            request=request,
            account_id = account_id,
            metadata_id = metadata.id, 
            user_id = request.token["user_id"],
            log_level=3,
            text = text[:100]
            )
    return getDocumentData(request, regno, [doc_id], account_id, company)
    
#dokumenta dzēšana
def deleteDocumentData(request, regno, doc_id, account_id, company):
    try:
        metadata = Metadata.objects.get(
                    id = doc_id, 
                    account = account_id,
                    company = company["id"]
                )
        storage = Storage.objects.get(
            id = metadata.storage.id
        )
    except:
        return error(400, "FIL_04") 
    if storage.deleted is None:
        try:
            #failu dzēšana (ja eksistē)
            os.remove(storage.file_path)
            os.remove(storage.filepath + ".txt")
        except:
            pass
        storage.deleted = datetime.now()
        storage.user_deleted = request.token["user_id"]
        storage.save()
        metadata.deleted = datetime.now()
        metadata.user_deleted = request.token["user_id"]
        metadata.save()
        logger(
            code = "DATA_DELETED", 
            request=request,
            account_id = account_id,
            storage_id = storage.id,
            metadata_id = metadata.id, 
            user_id = request.token["user_id"],
            log_level=3
            )
    return JsonResponse({"status": "deleted"})


#lejupielādēt oriģinālo datni
@api_view(['GET'])
@token_required
def downloadDocumentFile(request, regno, doc_id):
    account_id = request.token["account_id"]
    company = findCompany(account_id, regno)
    if company == False: return error(400, "USR_10")
    try:
        metadata = Metadata.objects.get(
                id = doc_id, 
                account = account_id,
                company = company["id"],
                deleted__isnull = True
            )
    except:
        logger(
            code = "DOWNLOAD_FAILED", 
            request=request,
            account_id = account_id,
            metadata_id = doc_id, 
            user_id = request.token["user_id"],
            log_level=2,
            text = "Metadata entry not found"
            )
        return error(400, "FIL_01")
    if (metadata.storage.deleted is None and
        metadata.storage.id is not None):
        try:
            f = open(metadata.storage.file_path, 'rb')
            file = File(f)
            response = HttpResponse(file, content_type=metadata.storage.content_type)
            response['Content-Disposition'] = 'attachment; filename="'+str(metadata.storage.filename)+'"'
            return response
        except:
            logger(
                code = "DOWNLOAD_FAILED", 
                request=request,
                account_id = account_id,
                storage_id = metadata.storage.id,
                metadata_id = metadata.id, 
                user_id = request.token["user_id"],
                log_level=2,
                text = "Download failed"
            )
            return error(400, "FIL_01")
    else:
        return error(400, "FIL_01")
    return JsonResponse({"regno": regno, "docid": doc_id, "account_id": account_id, "company": company})
