from functools import wraps
from django.conf import settings
from django.core.cache import cache
from .errors import *
import jwt, datetime

#piekļuves talona pārbaude
def token_required(fn):
    
    @wraps(fn)
    def _getStatus(request, *args, **kwargs):
        try:
            token = request.headers["Authorization"]
        except:
            return error(401, "AUT_01", "")
        try:
            token = token.replace("Bearer ", "")
            token_data = jwt.decode(token, settings.PRIVATE_KEY, algorithms=['HS256'])
            if datetime.datetime.fromtimestamp(token_data["exp"]) < datetime.datetime.now(): 
                return error(401, "AUT_02", "")
                #autentifikācijas talona pārbaude atmiņā
            key = "token_"+str(token_data["user"])
            tk_data = cache.get("token_"+str(token_data["user"]))
            if (tk_data is not None 
                and tk_data["salt"] == token_data["salt"] 
                and tk_data["ip_address"] == token_data["ip_address"]):
                    response = {
                        "user_id": token_data["user"], 
                        "account_id": tk_data["account_id"]
                        }
            else:
                return error(401, "AUT_02")

        except:
            return error(401, "AUT_02")
        request.token = response
        return fn(request, *args, **kwargs)
    
    return _getStatus