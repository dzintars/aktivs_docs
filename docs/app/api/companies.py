from django.shortcuts import render, redirect
from django.http import JsonResponse
from rest_framework.decorators import api_view
from docs.app.api.decorators import token_required
from django.core.cache import cache
from random import random
from docs.models.accounts import Companies, AccountUsers
from datetime import date
from .errors import *

#uzņēmuma ieraksta izveidošana
@api_view(['POST'])
@token_required
def createCompany(request):
    company_name = request.POST.get('company')
    regno = request.POST.get('regno')
    legal_address = request.POST.get('legal_address')
    vat_id = request.POST.get('vat_id')
    print(request.POST)
    if (company_name is None or company_name == '' or
        regno is None or regno == '' or
        legal_address is None):
        return error(400, "ERR_01")
    #saņem pārstāvētā konta datus
    try:
        account = AccountUsers.objects.get(
            user = request.token["user_id"],
            account_admin = True,
            valid = True
        )
    except:
        return error(400, "ERR_01")
    #pārbauda vai kontam jau ir piesaistīts uzņēmums ar šādu reģistrācijas numuru
    comp_count = Companies.objects.filter(
                account = account.account,
                regno = regno
                ).count()
    if comp_count>0: return error(400, "USR_09")
    if vat_id is None: vat_id = ''
    company = Companies(
            account = account.account,
            company = company_name,
            regno = regno,
            legal_address = legal_address,
            vat_id = vat_id,
            added = date.today()
    )
    company.save()
    response = {
        "company": company_name,
        "regno": regno,
        "legal_address": legal_address,
        "vat_id": vat_id
    }
    cacheCompanyList(account.account.id)
    return JsonResponse(response)

#uzņēmuma ieraksta labošana
@api_view(['POST'])
@token_required
def updateCompany(request):
    company_name = request.POST.get('company')
    regno = request.POST.get('regno')   #reģistrācijas numuru mainīt nevar, tiek meklēts eksistējošs uzņēmuma ieraksts
    legal_address = request.POST.get('legal_address')
    vat_id = request.POST.get('vat_id')
    if (company_name is None or company_name == '' or
        regno is None or regno == '' or 
        legal_address is None or legal_address == ''): 
            return error(400, "ERR_01")
    try:
        company = Companies.objects.get(
            regno = regno,
            account = request.token["account_id"]
        )
    except:
        return error(400, "USR_10")
    company.company = company_name
    company.legal_address = legal_address
    if vat_id is None: vat_id = ''
    company.vat_id = vat_id
    company.save()
    cacheCompanyList(request.token["account_id"])
    return JsonResponse({"status": "ok"})

#uzņēmuma ieraksta saņemšana
@api_view(['GET'])
@token_required
def getCompany(request, regno):
    if regno == '': return error(400, "ERR_01")
    try:
        company = Companies.objects.get(
            regno = regno,
            account = request.token["account_id"]
        )
    except:
        return error(400, "USR_10")
    response = {
        "company": company.company,
        "regno": company.regno,
        "legal_address": company.legal_address,
        "vat_id": company.vat_id
    }
    return JsonResponse(response)

#uzņēmuma saraksta saņemšana
@api_view(['GET'])
@token_required
def getCompanyList(request):
    account_id = request.token["account_id"]
    company_list = cache.get('company_list_'+str(account_id))
    if company_list is None:
        company_list = cacheCompanyList(account_id)
    return JsonResponse({"data": company_list})	

#saglabā kešatmiņā uzņēmumu sarakstu
def cacheCompanyList(account_id):
    companies = Companies.objects.filter(
            account = account_id
        )
    response_data = {}
    result_list = []
    for c in companies:
        result_list.append({
            "id": c.id,
            "company": c.company,
            "regno": c.regno,
            "legal_address": c.legal_address,
            "vat_id": c.vat_id
        })
    cache.set('company_list_'+str(account_id), result_list, 60*60)
    return result_list

#atrod kontam piesaistītu uzņēmumu
def findCompany(account_id, regno):
    company_list = cache.get('company_list_'+str(account_id))
    if company_list is None:
        company_list = cacheCompanyList(account_id)
    for c in company_list:
        if c["regno"] == regno:
            return c
    return False