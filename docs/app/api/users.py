#Reģistrācija, pieteikšanās u.c. darbības ar lietotājiem
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from django.conf import settings
from mailjet_rest import Client
import jwt, datetime, hashlib, re, requests, json, bcrypt
from datetime import timedelta, date
from .errors import *
from django.core.cache import cache
from docs.models.users import RefreshTokens
from docs.models.accounts import *
from django.contrib.auth.models import User
from docs.app.api.decorators import token_required
from random import random
from docs.app.tasks import sendEmail

#reģistrācija
@api_view(['POST'])
def signup(request):
    username = request.POST.get('username')
    first_name = request.POST.get('first_name')
    last_name = request.POST.get('last_name')
    email = request.POST.get('email')
    password = request.POST.get('password')
    password_2 = request.POST.get('password_2')
    terms_accepted = request.POST.get('terms_accepted')
    
    if password is None or password_2 is None or password!=password_2:
        return error(406, "USR_08")
    if terms_accepted is None:
        return error(406, "USR_05","terms_accepted")
    if username is None or len(username) < 5:
        return error(406, "USR_06","username")
    if first_name is None or len(first_name) < 2:
        return error(406, "USR_06","first_name")
    if last_name is None or len(last_name) < 2:
        return error(406, "USR_06","last_name")
    if email is None or len(email) < 5:
        return error(406, "USR_06","email")
    if password is None or len(password) < 8:
        return error(406, "USR_06","password")
    #tiek meklēts esošs lietotājs ar tādu pašu lietotāja vārdu
    #ja tāds netiek atrasts, tad tiek atgriezta kļūda
    try:
        user = User.objects.get(username=username)
        return error(406, "USR_07")
    except:
        pass   
    try:
        user = User.objects.get(email=email)
        return error(406, "USR_04")
    except:
        pass   
    data = {
        'username': username,
        'first_name': first_name,
        'last_name': last_name,
        'email': email,
        'password': password
    }
    code = int(random()*10000)
    key_id = "pending_registration_"+str(code)+"_"+str(username)
    cache.set(key_id, data, 60*60)
    if settings.DEBUG: 
        return JsonResponse({'code': str(code)})
    #e-pasta verifikācijas koda nosūtīšana uz e-pastu
    sendEmail.delay(first_name + ' ' + last_name, email, code)
    return JsonResponse({"status": "ok"})

#reģistrācijas apstiprināšana
@api_view(['GET'])
def signupValidate(request):
    code = request.GET.get('code')
    username = request.GET.get('username')
    if code is None or username is None:
        return error(400, "ERR_01")
    data = cache.get("pending_registration_"+str(code)+"_"+str(username))
    if data is None:
       return error(400, "ERR_01")
    try:
        db_user = User.objects.create_user(
                username = data["username"],
                email = data["email"],
                password = data["password"]
            )
        db_user.first_name = data["first_name"]
        db_user.last_name = data["last_name"]
        db_user.save()
        cache.delete("pending_registration_"+str(code)+"_"+str(username))
        #mēģinām lietotāju arī ielogot sistēmā
        user = authenticate(username=username, password=data["password"])
        #izveido kontu
        account = Account(
                    subscription_valid = date.today() + timedelta(days=30),
                    monthly_docs = 50,
                    payer_email = user.email
                )
        account.save()
        #izveido lietotāja saikni ar kontu
        account_users = AccountUsers(
            account = account,
            user = user.id
        )
        account_users.save()
        #izveido iestatījumu ierakstu
        acc_settings = AccountSettings(
            account = account
        )
        acc_settings.save()
        #izdod talonus
        if user is not None:
            token = issue_token(request, user, account.id)
            token = token.decode("utf-8")
            refreshToken = issue_token(request, user, refreshToken = True)
            refreshToken = refreshToken.decode("utf-8")
            response = {}
            response["accessToken"] = token
            response["refreshToken"] = refreshToken
            return JsonResponse(response, safe=False)
        else:
            return error(401, "USR_01") 
    except:
        return error(400, "ERR_01")

#lietotāja pārbaude
@api_view(['POST'])
def login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    if (username!=None and password!=None
      and username!="" and password!=""):
        user = authenticate(username=username, password=password)
        if user is not None:
            account = AccountUsers.objects.get(
                user = user.id
            )
            token = issue_token(request, user, account.account.id)
            token = token.decode("utf-8")
            refreshToken = issue_token(request, user, refreshToken = True)
            refreshToken = refreshToken.decode("utf-8")
            response = {}
            response["accessToken"] = token
            response["refreshToken"] = refreshToken
            return JsonResponse(response, safe=False)
        else:
            return error(401, "USR_01")      
    else:
        return error(403, "ERR_01")

#lietotāja talona atjaunošana
@api_view(['POST'])
def tokenRefresh(request):
    #iesūtīts viedreizējais pieejas atjaunošanas talons
    try:
        token = request.POST.get("token")
    except:
        return error(401, "AUT_01", "")
    try:
        token = token.replace("Bearer ", "")
        token_data = jwt.decode(token, settings.PRIVATE_KEY, algorithms=settings.JWT_ALGORITHM)
        if datetime.datetime.fromtimestamp(token_data["exp"]) < datetime.datetime.now(): 
            return error(401, "AUT_02", "")
        #pārbauda vienreizējā talona derīgumu datubāzē
        tk_data = RefreshTokens.objects.get(
            user_id=token_data["user"],
            salt=token_data["salt"]
        )
    except:
        return error(401, "AUT_02")
    user = User.objects.get(id=int(token_data["user"]))
    account = AccountUsers.objects.get(
        user = user.id
    )
    if user is not None:
        token = issue_token(request, user, account.account.id)
        token = token.decode("utf-8")
        refreshToken = issue_token(request, user, refreshToken = True)
        refreshToken = refreshToken.decode("utf-8")
        response = {}
        response["accessToken"] = token
        response["refreshToken"] = refreshToken
        return JsonResponse(response, safe=False)
    else:
        return error(401, "AUT_03") 

#atteikšanās no sistēmas
@api_view(['POST'])
@token_required
def logout(request):
    cache.delete("token_" + str(request.token["user_id"]))
    try:
        refreshToken = RefreshTokens.objects.get(user_id = request.token["user_id"])
        refreshToken.delete()
    except:
        pass
    return JsonResponse({"status": "ok"})

#paroles izgūšana
@api_view(['POST'])
@token_required
def changePassword(request):
    currentPassword = request.POST.get("old_password")
    newPassword = request.POST.get("new_password")
    newPassword2 = request.POST.get("new_password_2")
    if currentPassword is None or newPassword is None or newPassword2 is None:
        #nav visi obligātie lauki
        return error(403, "ERR_01")
    elif newPassword!=newPassword2:
        #jaunās paroles atšķiras
        return error(400, "USR_08")
    elif len(newPassword) < 8:
        #parole pārāk īsa
        return error(400, "USR_11")
    else:
        try:
            user = User.objects.get(id=request.token["user_id"])
        except:
            return error(400, "ERR_01")
        checkPassword = authenticate(username=user.username, password=currentPassword)
        if checkPassword is not None:
            user.set_password(newPassword)
            user.save()
        else:
           return error(400, "USR_12") 
    return JsonResponse({"status": "password changed"})    

#pieslēgšanās talona izdošana
def issue_token(request, user, account_id = 0, refreshToken = False):
    salt = str(bcrypt.gensalt())
    ip_address = request.META.get('REMOTE_ADDR')
    if refreshToken:
        #vienreizējā autentifikācijas talona izdošana
        exp = datetime.datetime.now() + datetime.timedelta(days=settings.REFRESH_TOKEN_EXP)
        try:
            #atrod eksistējošu ierakstu un to atjaunina (ja eksistē)
            refreshToken = RefreshTokens.objects.get(user_id = user.id)
            refreshToken.salt = salt
            refreshToken.issued = datetime.date.today()
            refreshToken.save()
        except:
            #talons netika atrasts, izveido jaunu ierakstu
            RefreshTokens(
                user_id = user.id,
                salt = salt, 
                issued = datetime.date.today()
            ).save()
    else:
        #vairāk lietojamā autentifikācijas talona izdošana
        exp = datetime.datetime.now() + datetime.timedelta(minutes=settings.TOKEN_EXP)
        cache.set('token_'+str(user.id),
                {
                    'salt': salt,
                    'ip_address': ip_address,
                    'account_id': account_id
                }
                ,60*settings.TOKEN_EXP)
    encoded = jwt.encode({
        'user': user.id, 
        'salt': salt,
        'ip_address': ip_address,
        'exp': exp
        }, settings.PUBLIC_KEY, algorithm=settings.JWT_ALGORITHM)
    return (encoded)

#token_required dekoratora tests
@api_view(['GET'])
@token_required
def getData(request):
    return JsonResponse({"data":request.token})
