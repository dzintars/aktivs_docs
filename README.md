# Finanšu dokumentu apstrādes sistēma

## Atkarības

* Python3.6+
* Node.js 14.2+ (Nepieciešams lietotāja interfeisa lietošanai izstrādes vidē un lietotāja interfeisa kompilēšanai)
* MySQL
* Redis
* Tesseract OCR ar tesseract latviešu valodas atbalsts (tesseract-ocr-lv)
```
sudo apt-get install tesseract-ocr tesseract-ocr-lav
```
* ocrmypdf
```
sudo apt-get install ocrmypdf
```
* Citas bibliotēkas, kuras definētas requirements.txt
```
pip install -r requirements.txt
```

## Lietojumprogrammas startēšana izstrādes vidē

Izstrādes vides startēšanai ir jāstartē 3.paralēlie procesi

### 1.Lietotāja interfeisa startēšana

Ja lietotāja interfeisu startē pēc Django izstrādes servera, tad klienta vide nav pieejama uz Django porta

```
cd frontend
npm run serve
```

### 2. Django servera startēšana

```
python3 manage.py runserver
```

### 3.Asinhrono uzdevumu izpildes servisa startēšana
```
celery -A aktivs_docs worker --loglevel=info
```

### 0.Datubāzes tabulu izveidošana

```
python3 manage.py makemigrations
python3 manage.py migrate
```

## Lietojumprogrammas konfigurācija produkcijas serverī

### Apache konfigurācija

Konfigurācijas piemērā lietojumprogramma atrodas "/home/dzintars/apps/docs/app/" direktorijā

```
<IfModule mod_ssl.c>
<VirtualHost *:443>
        ServerName docs.aktivs.lv
        ServerAdmin webmaster@localhost

        ErrorLog /home/dzintars/apps/docs/log/error.log
        CustomLog /home/dzintars/apps/docs/log/access.log combined

        WSGIProcessGroup django
        WSGIScriptAlias / /home/dzintars/apps/docs/app/aktivs_docs/wsgi.py
        WSGIPassAuthorization On

        Alias /favicon.ico /home/dzintars/apps/docs/app/templates/favicon.ico
        Alias /css/ /home/dzintars/apps/docs/app/templates/css/
        Alias /js/ /home/dzintars/apps/docs/app/templates/js/
        Alias /img/ /home/dzintars/apps/docs/app/templates/img/
        Alias /fonts/ /home/dzintars/apps/docs/app/templates/fonts/
        <Directory /home/dzintars/apps/docs/app/templates>
                Require all granted
        </Directory>

        <Directory /home/dzintars/apps/docs/app/aktivs_docs>
                <Files wsgi.py>
                        SetEnvIfNoCase Host aktivs\.lv VALID_HOST
                        #SetEnvIfNoCase Host .+ VALID_HOST
                        Order Deny,Allow
                        Deny from All
                        Allow from env=VALID_HOST
                        Require all granted
                </Files>
        </Directory>

        #Konfigurācija, lai būtu pieejams Django administratīvais panelis
        Alias /static/admin /home/dzintars/django-env/lib/python3.6/site-packages/django/contrib/admin/static/admin
        <Directory /home/dzintars/django-env/lib/python3.6/site-packages/django/contrib/admin/static/admin>
                Require all granted
        </Directory>

SSLCertificateFile /etc/letsencrypt/live/docs.aktivs.lv/fullchain.pem
SSLCertificateKeyFile /etc/letsencrypt/live/docs.aktivs.lv/privkey.pem
Include /etc/letsencrypt/options-ssl-apache.conf
</VirtualHost>
</IfModule>
```

### Lietotāja interfeisa konfigurācija

* Lietotāja interfeisu nepieciešāms kompilēt
```
cd frontend
npm run build
```

* Un pārvietot uz atbilstošo direktoriju
```
mv -v dist/* ../templates/
cd ../templates
mv application.html application_bck.html
mv index.html application.html
```

## Django konfigurācija

Repozitorijā Django konfigurācijai ir izveidota sagatave
```
cd aktivs_docs
cp settings.py.env settings.py
```

### Konfigurācijas parametri

* SECRET_KEY
Nepieciešams iegūt jaunu atslēgu, to var veikt [šeit](https://djecrety.ir/)

* DEBUG
Produkcijas vidē nepieciešams nomainīt uz False

* DATABASES
nepieciešams izvedot datubāzi un konfigurācijā norādīt tās parametrus
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', 
        'NAME': 'DBNAME',
        'USER': 'USERNAME',
        'PASSWORD': 'PASSWORD',
        'HOST': 'localhost', 
        'PORT': '3306'
    }
}
```

* PRIVATE_KEY

Privātā/publiskā atslēga un JWT izmantotai algoritms.
Atbalstītie algoritmi uzskaitīti [šeit](https://pypi.org/project/jwt/)
```
PRIVATE_KEY = """....PRIVATE KEY...."""
PUBLIC_KEY = PRIVATE_KEY
JWT_ALGORITHM = "HS256"
```
Paraugā ir HS256 algoritms, kurš izmanto vienu atslēgu

* CACHES

Redis servera konfigurācija
```
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient"
        },
        "KEY_PREFIX": "aktivs_docs"
    }
}
```

* Celery konfigurācija
(Celery izmanto Redis serveri, tādēļ konfigurācijā norādāma Redis servera parametri)
```
CELERY_BROKER_URL = 'redis://localhost:6379/1'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/1'
```

* E-pastu sūtīšanai tiek izmantots [Mailjet](http://www.mailjet.com) serviss

Konfigurācijā norāda atbilstošos piekļuves datus
```
MAILJET = {
    'API_KEY': "API_KEY",
    'API_SECRET': "API_SECRET",
    'MAIL_FROM_EMAIL': "dokumenti-noreplay@aktivs.lv",
    'MAIL_FROM': 'Dokumenti',
    'TEMPLATE_ID': 1259696
}
```

Mailjet jāizveido e-pasta veidne, kurā jāparedz šādi atribūti:
* first_name - saņēmēja vārds
* code - e-pasta validācijas kods



## Licence
[Apache License version 2](https://www.apache.org/licenses/LICENSE-2.0)

## Autors
Dzintars Spodris - [www.aktivs.lv](http://www.aktivs.lv)