import Login from './components/Login.vue'
import Logout from './components/Logout.vue'
import Signup from './components/Signup.vue'
import List from './components/List.vue'
import AddCompany from './components/AddCompany.vue'
import Upload from './components/Upload.vue'


export default [
  { path: '/', component: Login, name: "Home" },
  { path: '/signup', component: Signup, name: "Signup" },
  { path: '/login', component: Login, name: "Login" },
  { path: '/logout', component: Logout, name: "Logout" },
  { path: '/list', 
    component: List, 
    name: "List"
  },
  { path: '/upload', 
    component: Upload, 
    name: "File upload"
  },
  { path: '/settings', 
    component: List, 
    name: "Settings"
  },
  { path: '/add_company', 
    component: AddCompany, 
    name: "Add company"
  },
]