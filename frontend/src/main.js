import Vue from 'vue'
import VueRouter from 'vue-router'
import Routes from './routes'
import App from './App'
import vuetify from './plugins/vuetify';
import '@mdi/font/css/materialdesignicons.css'


Vue.config.productionTip = false
Vue.use(VueRouter)

const router = new VueRouter({
    routes: Routes
})

new Vue({
  vuetify,
  created: function(){
    
  },
  render: h => h(App),
  router

}).$mount('#app')
