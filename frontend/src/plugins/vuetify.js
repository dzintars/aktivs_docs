import Vue from 'vue';
import Vuetify from 'vuetify/lib';


Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#7CB342",
        secondary: "#33691E",
        accent: "#8BC34A"
      },
    },
  },
})